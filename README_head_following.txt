The visualisation software used for the previous demos (face_detectorExt) is outdated and does not work properly anymore.
New visualiation software (pi_vision/pi_face_tracker) will be used, but is not completely integrated yet.

To integrate this the find_face component needs to be adapted.

        The facesCloud port and variable now come from a different software package with a different type (geometry_msgs/PointStamped).
        This needs to be taken into account in the c++ code (findFace-component.cpp):
                - The frame facesCloud.header.frame_id will now correspond with kinect_depth_optical_frame.
                - facesCloud.points[closest].x/y/z now corresponds to facesCloud.point.x/y/z
                - Lines 98-107 are not necessary anymore.

        The facesCloud port now has to be connected with a different rostopic, corresponding with the different software package.
                - In earlier versions, connections were made in run.ops, but in itasc_dsl, a new driver is necessary.

Since this driver and component will also be used outside the PR2-context, it is still in development. Therefor the headtracking task is now not working yet. The head will remain in a fixed position.
