The obstacle_avoidance task is not fully operational yet.
The scripts and the cpf file for the obstacle avoidance should be working, but the code for the VKC is not.
This causes the task not to be actived when is specified.
There is an issue with the Chif_port used by the Constraint Controller. The cilindrical coordinates probably still have to be specified somewhere (in the DSL model?).
It is adviced not to enable this task in the model until the necessary bugfixes are done.
