my_tasks = {
   Task{
      name= "wrench_nulling_left",
      vkc = VKC{type="iTaSC::VKC_Cartesian", package="cartesian_motion",
                o1="pr2.root", o2="pr2.l_gripper_tool_frame"},
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion",
                prio_num=1, config={"file://dsl_comanipulation_demo#/cpf/CC_ForceNullingLeft.cpf"} },
      connect = {
       Rewire{src="left_twist_generator", tgt="cc",
           {from="all",to="all"}
        }
      },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}
   },
   Task{
    name= "wrench_nulling_right",
     vkc = VKC{type="iTaSC::VKC_Cartesian", package="cartesian_motion",
               o1="pr2.root", o2="pr2.r_gripper_tool_frame"},
     cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion",
                prio_num=1, config={"file://dsl_comanipulation_demo#/cpf/CC_ForceNullingRight.cpf"} },
      connect = {
        Rewire{src="right_twist_generator", tgt="cc",
           {from="all",to="all"}
        }
     },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}
  },
  Task{
      name = "pr2_joint_limit_avoidance",
      cc = CC{type= "iTaSC::CC_joint_limit_avoidance", package = "joint_limit_avoidance",
        prio_num=1, config={"file://dsl_comanipulation_demo#/cpf/pr2_jointlimits.cpf"}
      },
      robot = "pr2",
      fsm = FSM{fsm = "file://joint_limit_avoidance#/scripts/joint_limit_avoidance_supervisor.lua"}
   },
   Task{
      name= "grippers_parallel",
      vkc = VKC{type= "iTaSC::VKC_Cartesian", package="cartesian_motion",
                o1="pr2.l_gripper_tool_frame", o2="pr2.r_gripper_tool_frame"},
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion",
                prio_num=1, config={"file://dsl_comanipulation_demo#/cpf/CC_KeepParallel.cpf"} },
      connect = {
        Rewire{src="parallel_pose_generator", tgt="cc",
           {from="all",to="all"}
        }
      },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}
   },
--   Task{
--      name= "obstacle_avoidance",
--      vkc = VKC{type= "iTaSC::VKC_sixDof", package="sixDof_pff",
--                o1="obstacle.ee", o2="pr2.base_link",
--                config={"file://dsl_comanipulation_demo#/cpf/VKC_KeepDistance.cpf"}},
--      cc = CC{type="iTaSC::CC_sixDof_pff", package="sixDof_pff",
--                prio_num=1, config={"file://dsl_comanipulation_demo#/cpf/CC_KeepDistance.cpf"} },
--      connect = {
--        Rewire{src="keep_distance_generator", tgt="cc",
--           {from="all",to="all"}
--        }
--      },
--      fsm = FSM{fsm = "file://dsl_comanipulation_demo#/scripts/obstacle_avoidance_supervisor.lua",
--        config={"file://dsl_comanipulation_demo#/cpf/ObstacleAvoidanceSupervisor.cpf"}}
--   },
--   Task{
--      name= "head_tracking",
--      vkc = VKC{ type= "iTaSC::VKC_Cartesian", package="cartesian_motion",
--                 o1="pr2.head_plate_frame", o2="person.ee"},
--      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion",
--                 prio_num=1, config={"file://dsl_comanipulation_demo#/cpf/CC_HeadFollowing.cpf"} },
--      connect = {
--        Rewire{src="head_following_generator", tgt="cc",
--           {from="all",to="all"}
--        }
--      },
--      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}
--   }
}--end of tasks

return Application {
   dsl_version = '0.1',
   name = 'app_human_pr2_comanipulation_demo',
   uri = 'be.kuleuven.mech.robotics.application.human_pr2_comanipulation_demo',
   fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_application_supervisor.lua"},

   -- itasc
   itasc = iTaSC {
      dsl_version = '0.1',
      name = 'human_pr2_comanipulation_demo',
      uri = 'be.kuleuven.mech.robotics.itasc.human_pr2_comanipulation_demo',
      fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_itasc_supervisor.lua", 
                config = {"file://dsl_comanipulation_demo#/cpf/itasc_supervisor.cpf"}},

      -- robots
      robots = {
        Robot{
                name = "pr2",
                package = "itasc_pr2",
                type = "iTaSC::pr2Robot",
                driver = "pr2_driver",
                config = {"file://dsl_comanipulation_demo#/cpf/pr2robot.cpf"}
            },
      },

     -- objects
      objects = {
        Object{
                name = "obstacle",
                package = "fixed_object",
                type = "iTaSC::FixedObject",
                config = {"file://dsl_comanipulation_demo#/cpf/FixedObject.cpf"}
        },
--         Object{
--                name = "person",
--                package = "moving_object",
--                type = "iTaSC::MovingObject",
--                config = {"file://dsl_comanipulation_demo#/cpf/MovingObject.cpf"}
--        }
      },

      -- scene
      scene = {
             SceneElement {
                robot = "pr2",
                location = Frame{
                   M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
                   p = Vector{X=0.0,Y=0.0,Z=0.0}
                }
             },
            SceneElement {
                object = "obstacle",
                location = Frame {
                   M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
                   p = Vector{X=1.0,Y=0.0,Z=0.0}
                 }
             },
--             SceneElement {
--                object = "person",
--                location = ExternalInput{topic = "/find_face/face_world"}
--             }
      },

      tasks = my_tasks,

      solver = Solver{ name="Solver", package="wdls_prior_vel_solver", type="iTaSC::WDLSPriorVelSolver" }

   }, --end of iTaSC

   setpoint_generators = {
      SetpointGenerator{name="left_twist_generator",
                        type="trajectory_generators::tdFromForceGenerator",
                        package="td_from_force_generator",
                        file="file://td_from_force_generator#/scripts/td_from_force_generator.lua",
                        config={"file://dsl_comanipulation_demo#/cpf/twistGenerator.cpf"}
      },
      SetpointGenerator{name="right_twist_generator",
                        type="trajectory_generators::tdFromForceGenerator",
                        package="td_from_force_generator",
                        file="file://td_from_force_generator#/scripts/td_from_force_generator.lua",
                        config={"file://dsl_comanipulation_demo#/cpf/twistGenerator.cpf"}
      },
      SetpointGenerator{name="parallel_pose_generator",
                        type="trajectory_generators::FixedPoseGenerator",
                        package="fixed_pose_generator",
                        config={"file://dsl_comanipulation_demo#/cpf/ParallelPoseGenerator.cpf"}
      },
--      SetpointGenerator{name="keep_distance_generator",
--                          type="trajectory_generators::SimpleGenerator6D",
--                          package="simple_generator_6d",
--                          config={"file://dsl_comanipulation_demo#/cpf/KeepDistanceGenerator.cpf"}
--      },
--      SetpointGenerator{name="head_following_generator",
--                            type="trajectory_generators::FixedPoseGenerator",
--                            package="fixed_pose_generator",
--                            config={"file://dsl_comanipulation_demo#/cpf/HeadFollowingGenerator.cpf"} 
--      },
   },--end of setpoint_generators 

   drivers = {
      Driver{name="pr2_driver", package="itasc_pr2", file="file://itasc_pr2#/scripts/pr2driver.lua"}
   }
} --end of application


