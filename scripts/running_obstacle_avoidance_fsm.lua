--           This file is part of the iTaSC project                      
--                                                                       
--                  (C) 2011 Dominick Vanthienen                         
--                  (C) 2011 Tinne De Laet                                 
--              dominick.vanthienen@mech.kuleuven.be,                    
--                  tinne.delaet@mech.kuleuven.be,                    
--              Department of Mechanical Engineering,                    
--             Katholieke Universiteit Leuven, Belgium.                  
--                    http://www.orocos.org/itasc                  
--                                                                       
-- You may redistribute this software and/or modify it under either the  
-- terms of the GNU Lesser General Public License version 2.1 (LGPLv2.1  
-- <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>) or (at your 
-- discretion) of the Modified BSD License:                              
-- Redistribution and use in source and binary forms, with or without    
-- modification, are permitted provided that the following conditions    
-- are met:                                                              
-- 1. Redistributions of source code must retain the above copyright     
-- notice, this list of conditions and the following disclaimer.         
-- 2. Redistributions in binary form must reproduce the above copyright  
-- notice, this list of conditions and the following disclaimer in the   
-- documentation and/or other materials provided with the distribution.  
-- 3. The name of the author may not be used to endorse or promote       
-- products derived from this software without specific prior written    
-- permission.                                                           
-- THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR  
-- IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED        
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE    
-- ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS       
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,   
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
-- IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE    
-- POSSIBILITY OF SUCH DAMAGE.                                           
-- sixDof_pffFSM
-- runningsixDof_pffFSM

return rfsm.composite_state{
--dbg=fsmpp.gen_dbgcolor2("TaskFSM"),

NONemergency = rfsm.composite_state{
	idle = rfsm.simple_state{
                entry=function(fsm)
                        print("=>" .. ComponentName .. "FSM=>Running=>FSM->idle state")
                        deactivateTask()
                end,
        },

        AvoidObstacle = rfsm.simple_state{
                entry=function(fsm)
                        print("=>" .. ComponentName .. "FSM=>Running=>FSM-> AvoidObstacle state entry")
                        deactivateTask()
                end,
                exit=function()
                        deactivateTask()
                end,
        doo=function(fsm)
            while true do
                --print (" FSM=>Running=>FSM-> AvoidObstacle doo function")
                -- read chif_distance 
                local fs, data = VKC_ObstacleAvoidance_chif_distance_in:read()
                -- print("data received: " .. tostring(data) .. "flowstatus: " .. fs )
                -- if closer than predefined value
                local minimumDistance = 0.4 -- 1
                local desiredDistance = 0.5  -- 1.1
                if ( data < minimumDistance and data>-minimumDistance  ) then
                    -- print(" too close ")
                    if (data > 0) then
                        -- set desired value to + desiredDistance
                        ObstacleAvoidanceGeneratorSetDesiredSetPoint(1,desiredDistance)
                    else 
                        -- set desired value to - desiredDistance
                        ObstacleAvoidanceGeneratorSetDesiredSetPoint(1,-desiredDistance)
                    end
                    -- turn control on
                                activateTask()
                else    
                    -- print(" far enough")
                    -- turn control off
                                deactivateTask()
                end             
            rfsm.yield(true)
            end     
        end,        
        },


	DoNothing = rfsm.simple_state{
		entry=function(fsm)
			print("=>" .. ComponentName .. "_fsm=>Running=>FSM-> DoNothing state entry")
			deactivateTask()
		end,
	},

        rfsm.transition { src='initial', tgt='idle' },
        rfsm.transition { src='idle', tgt='AvoidObstacle', events={'e_' .. ComponentName .. '_all_tasks_activate'} },
        rfsm.transition { src='idle', tgt='DoNothing', events={'e_' .. ComponentName .. '_DoNothing'} },
        rfsm.transition { src='AvoidObstacle', tgt='DoNothing', events={'e_' .. ComponentName .. '_DoNothing'} },
        rfsm.transition { src='DoNothing', tgt='AvoidObstacle', events={'e_' .. ComponentName .. '_all_tasks_activate'} },
},


runningsixDof_pffFSMemergency= rfsm.simple_state{
	entry = function ()
		deactivateTask()
		raise_priority_event("e_emergency")
		print("[EMERGENCY] =>" .. ComponentName .. "_fsm=>Running=>FSM-> encountered an error!")
		print("[EMERGENCY] =>" .. ComponentName .. "_fsm=>Running=>FSM-> therefore raised an e_emergency event")
	end,
},

rfsm.transition { src='initial', tgt='NONemergency' },
rfsm.transition { src='NONemergency', tgt='runningsixDof_pffFSMemergency', events={'e_emergency'} },
}

