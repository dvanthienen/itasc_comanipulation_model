--           This file is part of the iTaSC project
--
--                  (C) 2011 Dominick Vanthienen
--                  (C) 2011 Tinne De Laet
--              dominick.vanthienen@mech.kuleuven.be,
--                  tinne.delaet@mech.kuleuven.be,
--              Department of Mechanical Engineering,
--             Katholieke Universiteit Leuven, Belgium.
--                    http://www.orocos.org/itasc
--
-- You may redistribute this software and/or modify it under either the
-- terms of the GNU Lesser General Public License version 2.1 (LGPLv2.1
-- <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>) or (at your
-- discretion) of the Modified BSD License:
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
-- notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
-- 3. The name of the author may not be used to endorse or promote
-- products derived from this software without specific prior written
-- permission.
-- THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
-- IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
-- IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-- sixDof_pffFSM
require "rttlib"
require "rfsm"
require "rfsm_rtt"
require "rfsm_ext" 	--needed for the sequential-AND state
require "rfsmpp"	 	--needed for the sequential-AND state
require "kdlpp"	 	--kdl pretty print (should be included in lua path!)
require "rttros"	--needed for 'find_rospack'

tc=rtt.getTC()
local common_events_in, priority_events_in, trigger_events_in
local checkResponse
TaskName = "sixDof_pff" -- name of the task package
equality = true

--- function to create table with properties
-- (= can only be read from .cpf file AFTER configureHook is executed
-- => this separate function, should be called when in configuring state of taskFSM )
function getFSMproperties()
	FSMproperties={}
	return true
end

--- function to check validity of FSM properties
function checkFSMproperties()
	return true
end


function configureHook()

	-- peer table (to enable smaller code to request operations)
	sixDof_pffPeertable = rttlib.mappeers(function (tc) return tc end, tc)
	ComponentName =  string.gsub(tc:getName(),"_supervisor","")
	--print("[" .. ComponentName .. "_supervisor.lua] SuperVisor has following peers:")
	--for K,V in pairs(sixDof_pffPeertable) do print( K) end

	-- PROPERTIES
	-- location of the FSM of this task
    task_fsm_package_prop=rtt.Property("string","task_fsm_package","package where to find the task_fsm, if any")
    tc:addProperty(task_fsm_package_prop)
	task_fsm_prop=rtt.Property("string", "task_fsm", "path and name to the FSM file of this task, starting from the package (start with a slash), if any")
	tc:addProperty(task_fsm_prop)
	-- location of the running_task_FSM of this task
    running_task_fsm_package_prop=rtt.Property("string","running_task_fsm_package","package where to find the running_task_fsm, if any")
    tc:addProperty(running_task_fsm_package_prop)
	running_task_fsm_prop=rtt.Property("string", "running_task_fsm", "path and name to the running_task_FSM file of this task, starting from the package (start with a slash), if any")
	tc:addProperty(running_task_fsm_prop)
	-- location of the running_task_coordination of this task
    running_task_coordination_package_prop=rtt.Property("string","running_task_coordination_package","package where to find the running_task_coordination, if any")
    tc:addProperty(running_task_coordination_package_prop)
	running_task_coordination_prop=rtt.Property("string", "running_task_coordination", "path and name to the running_task_fsm_prop file of this task, starting from the package (start with a slash), if any")
	tc:addProperty(running_task_coordination_prop)

    -- fill in standard values for the properties (this will cause it to work as in previous versions)
    task_fsm_package_prop:set("")
    running_task_fsm_package_prop:set("")
    running_task_coordination_package_prop:set("")
	task_fsm_prop:set(rttros.find_rospack(TaskName) .. "/scripts/" .. TaskName .. "_fsm.lua")
    running_task_coordination_prop:set(rttros.find_rospack(TaskName) .."/scripts/running_"..TaskName .. "_coordination.lua")
    running_task_fsm_prop:set(rttros.find_rospack(TaskName) .."/scripts/running_" .. TaskName .. "_fsm.lua")

    -- OPERATIONS
    ObstacleAvoidanceGeneratorSetDesiredSetPoint = sixDof_pffPeertable.keep_distance_generator:getOperation("setDesiredSetpoint")

	-- INPUT PORTS

	-- COMMON events: the following creates a string input port, adds it as a
	-- port to the Taskcontext.
	common_events_in = rtt.InputPort("string")
	tc:addPort(common_events_in, ComponentName .. "_common_events_in", ComponentName .. " common_event input port")

	-- PRIORITY events: the following creates a string input port, adds it as an event
	-- driven port to the Taskcontext.
	priority_events_in = rtt.InputPort("string")
	tc:addEventPort(priority_events_in, ComponentName .. "_priority_events_in", ComponentName .. " priority_event input port")

	-- TRIGGER events: the following creates a string input port, adds it as an event
	-- driven port to the Taskcontext.
	trigger_events_in = rtt.InputPort("string")
	tc:addEventPort(trigger_events_in, ComponentName .. "_trigger_events_in", ComponentName .. " trigger_event input port")

        -- port to read the chif_distance coordinates
        VKC_ObstacleAvoidance_chif_distance_in = rtt.InputPort("double")
        tc:addPort(VKC_ObstacleAvoidance_chif_distance_in, ComponentName .. "_chif_distance_in", ComponentName .. "chif_distance input port")

	--OUTPUT PORTS

	-- create a string port with which the current COMMON events are send
	common_events_out = rtt.OutputPort("string")
	tc:addPort(common_events_out, ComponentName .. "_common_events_out", "current common_events in sixDof_pffFSM")

	-- create a string port with which the current PRIORITY events are send
	priority_events_out = rtt.OutputPort("string")
	tc:addPort(priority_events_out, ComponentName .. "_priority_events_out", "current priority_events in sixDof_pffFSM")

	-- create a string port with which the current TRIGGER events are send
	trigger_events_out = rtt.OutputPort("string")
	tc:addPort(trigger_events_out, ComponentName .. "_trigger_events_out", "current trigger_events in sixDof_pffFSM")

	-- port to change Wy_global of sixDof_pff subTask
	CC_sixDof_pff_Wy_global_port = rtt.OutputPort("double","CC_" .. ComponentName .. "_Wy_global_port")

	-- Functions containing RTT specific info to request operations
	sixDof_pffUpdateVKCE = sixDof_pffPeertable["VKC_" .. ComponentName]:getOperation("updateVKCE")
	sixDof_pffUpdateOutputEq = sixDof_pffPeertable["CC_" .. ComponentName]:getOperation("updateOutputEquation")
	sixDof_pffUpdateController = sixDof_pffPeertable["CC_" .. ComponentName]:getOperation("updateController")

	return true
end

function startHook()
    print("[" .. ComponentName .. "_supervisor.lua] starting")
    -- getting the file locations
    if(running_task_fsm_package_prop:get()=="")
    then
        print("[" .. ComponentName .. "_supervisor.lua] No running_task_fsm_package specified, will look for fsm file on location specified by running_task_fsm property")
        running_task_fsm_file = running_task_fsm_prop:get()
    else
        running_task_fsm_file = rttros.find_rospack(running_task_fsm_package_prop:get()) .. running_task_fsm_prop:get()
    end
    if(running_task_coordination_package_prop:get()=="")
    then
        print("[" .. ComponentName .. "_supervisor.lua] No running_task_coordination_package specified, will look for coordination file on location specified by running_task_coordination property")
        running_task_coordination_file = running_task_coordination_prop:get()
    else
        running_task_coordination_file = rttros.find_rospack(running_task_coordination_package_prop:get()) .. running_task_coordination_prop:get()
    end
    if(task_fsm_package_prop:get()=="")
    then
        print("[" .. ComponentName .. "_supervisor.lua] No task_fsm_package specified, will look for fsm file on location specified by task_fsm property")
        task_fsm_file = task_fsm_prop:get()
    else
        task_fsm_file = rttros.find_rospack(task_fsm_package_prop:get()) .. task_fsm_prop:get()
    end

	-- load state machine
	print("[" .. ComponentName .. "_supervisor.lua] loading FSM: " .. task_fsm_file )
	fsm = rfsm.init(rfsm.load(task_fsm_file))
    --rfsm.pre_step_hook_add(fsm, function(fsm, events)
    --  if #events > 1 then print("sixDof_pff_supervisor received: "..utils.tab2str(events)) end
    --end)

	-- get all events from the all input ports
	-- getevents function, which returns all data on the current port as
	-- events. This function is called by the rFSM core to check for new events.
	fsm.getevents = rfsm_rtt.gen_read_str_events(common_events_in, priority_events_in, trigger_events_in)

	-- optional: create a string port to which the currently active
	-- state of the FSM will be written. gen_write_fqn generates a
	-- function suitable to be added to the rFSM step hook to do this.
	fqn_out = rtt.OutputPort("string")
	tc:addPort(fqn_out, "currentState", "current active rFSM state")
	fsm.step_hook=rfsm_rtt.gen_write_fqn(fqn_out)

	--raise event functions
	raise_common_event=rfsm_rtt.gen_raise_event(common_events_out, fsm)
	raise_priority_event=rfsm_rtt.gen_raise_event(priority_events_out, fsm)
	raise_trigger_event=rfsm_rtt.gen_raise_event(trigger_events_out)

    return true
end

function updateHook()
	-- this is a event driven component, triggered by ITASCFSM (on the priority_event_port), no timer needed
	rfsm.run(fsm)
end

function cleanupHook()
	-- cleanup the created ports.
	--tc:removePort(CartMotionInitPort:info().name)
	tc:removePort(common_events_in:info().name)
	tc:removePort(priority_events_in:info().name)
	tc:removePort(trigger_events_in:info().name)
	tc:removePort(common_events_out:info().name)
	tc:removePort(priority_events_out:info().name)
	tc:removePort(trigger_events_out:info().name)
	-- cleanup created variables
	common_events_in:delete()
	priority_events_in:delete()
	trigger_events_in:delete()
	common_events_out:delete()
	priority_events_out:delete()
	trigger_events_out:delete()
end

--- Function containing RTT specific info to configure the task
function configureTask()
	if sixDof_pffPeertable["VKC_" .. ComponentName]:configure()
	then print("   VKC_" .. ComponentName .. " configured")
	else
		print("   unable to configure VKC_" .. ComponentName)
		raise_common_event("e_emergency")
	end
	if sixDof_pffPeertable["CC_" .. ComponentName]:configure()
	then print("   CC_" .. ComponentName .. " configured")
	else
		print("   unable to configure CC_" .. ComponentName)
	 	raise_common_event("e_emergency")
	end
end

--- Function containing RTT specific info to start the task
function startTask()
	if sixDof_pffPeertable["CC_" .. ComponentName]:initialize()
	then print("   CC_" .. ComponentName .. " initialized")
	else
		print("   unable to initialize CC_" .. ComponentName)
		raise_common_event("e_emergency")
	end
	if CC_sixDof_pff_Wy_global_port:connect(sixDof_pffPeertable.Scene:getPort("CC_" .. ComponentName .. "_Wy_global"))
	then print("   CC_" .. ComponentName .. "_Wy_global_port connected")
	else
		print("   unable to connect CC_" .. ComponentName .. "_Wy_global_port")
	 	raise_common_event("e_emergency")
	end
end

--- Function containing RTT specific info to activate a task
function activateTask()
	CC_sixDof_pff_Wy_global_port:write(1.0)
end

--- Function containing RTT specific info to deactivate a task
function deactivateTask()
	CC_sixDof_pff_Wy_global_port:write(0.0)
end
